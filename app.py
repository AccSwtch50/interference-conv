import os
import time
import json
import random
from requests import get
from requests import post
from requests import utils
import openai

import g4f
from flask import Flask, request, Response, jsonify
from flask_cors import CORS
from werkzeug.exceptions import abort

app = Flask(__name__)
CORS(app)

models_data = []

config_file_path = "config.json"

with open(config_file_path, "r") as config_file:
    config_data = json.load(config_file)

def nova_app_call(model, messages, temperature, streaming):
  user_id = ''.join(random.choices('0123456789abcdef', k=32))
  X_stream = str(streaming).lower()
  if model == "gpt-4-0613":
    X_model = '2'
  elif model == "gpt-3.5-turbo-0613":
    X_model = '0'
  else:
    X_model = '1'
  return post(url = 'https://api.novaapp.ai/api/chat',
                  headers = {
                    'X_stream': X_stream,
                    'X_platform': 'android',
                    'X_user_id': user_id,
                    'X_pr': 'true',
                    'X_dev': 'false',
                    'X_model': X_model,
                    'User-Agent': 'okhttp/5.0.0-alpha.11'
                   },
                   json = {
                     'model' : 'gpt-3.5-turbo',
                     'messages' : messages,
                     'temperature' : temperature
                   },
             stream = streaming
             )

def hazi_call(model, messages, temperature):
  return post(url = 'https://hazi-response.vercel.app/api/generate/chat',
  headers = {
    'User-Agent': 'Gola/25 CFNetwork/1408.0.4 Darwin/22.5.0'
  },
  json = {
    'model' : model,
    'messages' : messages,
    'temperature' : temperature
    },
  stream = False
  )

def add_model(id, object, owned_by, tokens, type, permission):
  model = {
    "id": id,
    "object": object,
    "owned_by": owned_by,
    "tokens": tokens,
    "type": type,
    "permission": permission
  }
  models_data.append(model)


add_model("gpt-3.5-turbo", "model", "openai", 4000, "chat", [])
add_model("gpt-4", "model", "openai", 8000, "chat", [])
add_model("gpt-4-0613", "model", "openai", 8000, "chat", [])
add_model("gpt-4-0314", "model", "openai", 8000, "chat", [])
add_model("text-davinci-003", "model", "openai", 2000, "completion", [])
add_model("text-davinci-002", "model", "openai", 2000, "completion", [])
add_model("text-davinci-001", "model", "openai", 1000, "completion", [])
add_model("davinci-instruct-beta", "model", "openai", 1000, "completion", [])
add_model("davinci", "model", "openai", 1000, "completion", [])
add_model("bing", "model", "microsoft", 8000, "chat", [])


@app.before_request
def check_authorization():
  if 'Authorization' not in request.headers:
    abort(401)  # Unauthorized
  # Retrieve the token from the Authorization header
  auth_header = request.headers.get('Authorization')
  #print(auth_header)
  token = auth_header.split('Bearer ')[
    1]  # Extract the token from "Bearer <token>"

  key_array = [os.environ['SELF_KEY']]

  if not token in key_array:
    abort(403)  # Forbidden


@app.route("/v1/models", methods=['GET'])
def models_list():
  data = {"data": models_data, "object": "list"}
  return jsonify(data)


@app.route("/v1/chat/completions", methods=['POST'])
def chat_completions():
  streaming = request.json.get('stream', False)
  model = request.json.get('model', 'gpt-3.5-turbo')
  messages = request.json.get('messages')
  temperature = request.json.get('temperature')

  models = {
    'gpt-3.5-turbo': 'gpt-3.5-turbo',
    'gpt-4': 'gpt-4',
    'bing': 'bing',
    'gpt-4-0613': 'gpt-4-0613',
    'gpt-4-0314': 'gpt-4-0314'
  }

  dummy_key = 'sk-1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKL'
  gpt4_provider = config_data['gpt4_provider']
  purgpt_key = os.environ['PURGPT_KEY']
  streaming_models = ['gpt-3.5-turbo']
  streaming_gpt4_providers = ['nova_app']
  if gpt4_provider == "nova_app":
    streaming_models += ['gpt-4', 'gpt-4-0613']
  gpt_4_equiv = ['gpt-4', 'gpt-4-0613']
  march_14_models = ['gpt-4-0314', 'gpt-4-32k-0314']

  if models[model] == "gpt-3.5-turbo":
    response = g4f.ChatCompletion.create(model='gpt-3.5-turbo',
                                     provider=g4f.Provider.GetGpt,
                                     messages=messages,
                                     stream=streaming)
  elif models[model] in gpt_4_equiv:
    if gpt4_provider == "nova_app":
      response = nova_app_call("gpt-4-0613", messages, temperature, streaming)
    elif gpt4_provider == "hazi":
      response = hazi_call(models[model], messages, temperature)
    elif gpt4_provider == "g4f":
      response = g4f.ChatCompletion.create(model='gpt-4', provider=g4f.Provider.Bing, messages=messages, stream=streaming)
  elif models[model] == "bing":
    if temperature < 0.4:
      bing_mode = "Precise"
    elif 0.4 <= temperature < 0.7:
      bing_mode = "Balanced"
    else:
      bing_mode = "Creative"
    bing_url = 'https://purgpt.xyz/v1/bing' + "?message=" + messages[-1][
      'content'] + "&key=" + purgpt_key + "&variant=" + bing_mode
    response = post(url=bing_url)
  elif models[model] in march_14_models:
    response = hazi_call(models[model], messages, temperature)
  """response = post(url = api_url,
                headers = {'Authorization': 'Bearer %s' % api_key},
                json = {
                    'model' : models[model],
                    'messages' : messages,
                    'stream' : streaming
                    },
                stream = streaming
                )
    """

  if not streaming:
    while 'curl_cffi.requests.errors.RequestsError' in response:
      if models[model] == "gpt-3.5-turbo":
        response = ChatCompletion.create(model='gpt-3.5-turbo',
                                         provider=Provider.GetGpt,
                                         messages=messages,
                                         stream=streaming)
      elif models[model] in gpt_4_equiv:
        if gpt4_provider == "nova_app":
          response = nova_app_call("gpt-4-0613", messages, temperature, streaming)
        elif gpt4_provider == "hazi":
          response = hazi_call(models[model], messages, temperature)
      elif models[model] == "bing":
        if temperature < 0.4:
          bing_mode = "Precise"
        elif 0.4 <= temperature < 0.7:
          bing_mode = "Balanced"
        else:
          bing_mode = "Creative"
        bing_url = 'https://purgpt.xyz/v1/bing' + "?message=" + utils.quote(
          messages[-1]
          ['content']) + "&key=" + purgpt_key + "&variant=" + bing_mode
        response = post(url=bing_url)
      elif models[model] in march_14_models:
        response = hazi_call(models[model], messages, temperature)

    completion_timestamp = int(time.time())
    completion_id = ''.join(
      random.choices(
        'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789',
        k=28))

    if models[model] == "gpt-3.5-turbo":
      message_response = response
    elif models[model] in gpt_4_equiv:
      if gpt4_provider == "nova_app":
        message_response = json.loads(response.text)["choices"][0]["messages"]["content"]
      elif gpt4_provider == "hazi":
        message_response = json.loads(response.text)["answer"]
      elif gpt4_provider == "g4f":
        message_response = response
    elif models[model] == "bing":
      message_response = json.loads(response.text)["text"]
    elif models[model] in march_14_models:
      message_response = json.loads(response.text)["answer"]

    return json.dumps({
      'id':
      'chatcmpl-%s' % completion_id,
      'object':
      'chat.completion',
      'created':
      completion_timestamp,
      'model':
      models[model],
      'usage': {
        'prompt_tokens': None,
        'completion_tokens': None,
        'total_tokens': None
      },
      'choices': [{
        'message': {
          'role': 'assistant',
          'content': message_response
        },
        'finish_reason': 'stop',
        'index': 0
      }]
    })

  def stream():

    #int_response = response.data
    #message_response = json.dumps(int_response)['choices'][0]['delta']['content']

    token = ''
    if models[model] in gpt_4_equiv:
      if gpt4_provider == "nova_app":
        streamed_response = response.iter_lines()
      elif gpt4_provider == "hazi":
        streamed_response = response
      elif gpt4_provider == "g4f":
        streamed_response = response
    else:
      streamed_response = response
    for event in streamed_response:
      if models[model] == "gpt-3.5-turbo":
        token = event
      if models[model] in gpt_4_equiv:
        if gpt4_provider == "hazi":
          token = json.loads(response.text)["answer"]
        elif gpt4_provider == "nova_app":
          if b'content' in event:
            token = json.loads(event.decode().split('data: ')
                               [1])["choices"][0]["delta"]["content"]
          else:
            token = ''
        elif gpt4_provider == "g4f":
          token = event
      elif models[model] == "bing":
        token = json.loads(response.text)["text"]
      elif models[model] in march_14_models:
        token = json.loads(response.text)["answer"]
      if token:
        completion_timestamp = int(time.time())
        completion_id = ''.join(
          random.choices(
            'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789',
            k=28))

        completion_data = {
          'id':
          f'chatcmpl-{completion_id}',
          'object':
          'chat.completion.chunk',
          'created':
          completion_timestamp,
          'model':
          models[model],
          'choices': [{
            'delta': {
              'content': token
            },
            'index': 0,
            'finish_reason': None
          }]
        }

        yield 'data: %s\n\n' % json.dumps(completion_data,
                                          separators=(','
                                                      ':'))
        if models[model] in streaming_models:
          time.sleep(0.01)
        else:
          break
    yield 'data: [DONE]'

  return app.response_class(stream(), mimetype='text/event-stream')


@app.route("/v1/completions", methods=['POST'])
def completions():
  streaming = request.json.get('stream', False)
  model = request.json.get('model', 'text-davinci-003')
  prompt = request.json.get('prompt')
  temperature = request.json.get('temperature')
  max_tokens = request.json.get('max_tokens')
  stop_tokens = request.json.get('stop')

  models = {
    'text-davinci-003': 'text-davinci-003',
    'text-davinci-002': 'text-davinci-002',
    'text-davinci-001': 'text-davinci-001',
    'davinci-instruct-beta': 'davinci-instruct-beta',
    'davinci': 'davinci'
  }

  dummy_key = 'sk-1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKL'
  purgpt_key = os.environ['PURGPT_KEY']
  novaoss_key = os.environ['NOVAOSS_KEY']
  chimeragpt_key = os.environ['CHIMERAGPT_KEY']

  if models[model] == "text-davinci-003":
    api_url = 'https://chimeragpt.adventblocks.cc/api/v1'
    api_key = chimeragpt_key
    streaming_model = streaming
  else:
    api_url = 'https://api.nova-oss.com/v1'
    api_key = novaoss_key
    streaming_model = False

  openai.api_base = api_url
  openai.api_key = api_key
  response = openai.Completion.create(model=models[model],
                                      prompt=prompt,
                                      max_tokens=max_tokens,
                                      stream=streaming_model,
                                      stop=stop_tokens,
                                      temperature=temperature)
  """response = post(url = api_url,
                headers = {'Authorization': 'Bearer %s' % api_key},
                json = {
                    'model' : models[model],
                    'messages' : messages,
                    'stream' : streaming
                    },
                stream = streaming
                )
    """

  if not streaming:
    while 'curl_cffi.requests.errors.RequestsError' in response:
      response = openai.Completion.create(model=models[model],
                                          prompt=prompt,
                                          max_tokens=max_tokens,
                                          stream=streaming_model,
                                          stop=stop_tokens,
                                          temperature=temperature)

    completion_timestamp = int(time.time())
    completion_id = ''.join(
      random.choices(
        'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789',
        k=28))

    message_response = response['choices'][0]['text']

    return json.dumps({
      'id':
      'cmpl-%s' % completion_id,
      'object':
      'text_completion',
      'created':
      completion_timestamp,
      'model':
      models[model],
      'usage': {
        'prompt_tokens': None,
        'completion_tokens': None,
        'total_tokens': None
      },
      'choices': [{
        'text': message_response,
        'finish_reason': 'stop',
        'index': 0
      }]
    })

  def stream():

    #int_response = response.data
    #message_response = json.dumps(int_response)['choices'][0]['delta']['content']

    token = ''
    for event in response:
      if streaming_model == True:
        event_text = event['choices'][0]
        token = event_text.get('text', '')
      else:
        token = response['choices'][0]['text']
      if token:
        completion_timestamp = int(time.time())
        completion_id = ''.join(
          random.choices(
            'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789',
            k=28))

        completion_data = {
          'id': f'cmpl-{completion_id}',
          'object': 'text_completion',
          'created': completion_timestamp,
          'model': models[model],
          'choices': [{
            'text': token,
            'index': 0,
            'finish_reason': None
          }]
        }

        yield 'data: %s\n\n' % json.dumps(completion_data,
                                          separators=(','
                                                      ':'))
        if streaming_model == True:
          time.sleep(0.01)
        else:
          break

    yield 'data: [DONE]'

  return app.response_class(stream(), mimetype='text/event-stream')


if __name__ == '__main__':
  config = {'host': '0.0.0.0', 'port': 1337, 'debug': True}

  app.run(**config)
